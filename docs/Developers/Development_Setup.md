# Development Setup Notes

Following are the instructions to setup GitMate-2 on your local machine for development.

## Using Virtual Environment

We highly recommend you to setup and run GitMate-2 in a virtual environment.
This will allow you to avoid any version conflicts with other installations.
The virtual environment can be created using `venv` or `virtualenv`.
We recommend using `venv` as it is part of the standard library and requires no extra installation.
Refer to [venv docs](https://docs.python.org/3/library/venv.html).

!!! warning
    Make sure a Python 3 virtual environment is created and active before proceeding further.

## Cloning Relevant Repositories

After the virtual environment is setup, you need to fork and clone the `gitmate-2` and
`gitmate-2-frontend` repositories on your local machine.

To clone the repositories run
```bash
$ # clone gitmate-2
$ git clone <path/to/your/gitmate-2/fork>
$ # clone gitmate-2-frontend
$ git clone <path/to/your/gitmate-2-frontend/fork>
```
Set `git remote` configuration after cloning.
Refer to [Git Basics - Working with Remotes](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes).

## Setup GitMate-2 Backend

#### Development Requirements

Following are the development requirements for gitmate-2:

- Python3(v3.5 or above)
- [Django](https://www.djangoproject.com/)(v1.9 or above)
- [PostgreSQL](https://www.postgresql.org/about/)
- [Celery](http://docs.celeryproject.org/en/latest/index.html)
- [rabbitmq](https://www.rabbitmq.com)

To install gitmate requirements(except PostgreSQL), change the working directory to gitmate-2 and
run

```bash
$ python3 -m pip install -r requirements.txt
```

!!! note
    To install and properly set up PostgreSQL, refer to their docs on how to set it up.

#### Configuring Environment Variables

GitMate uses some environment variables in development. Make sure you set them correctly.

- `DJANGO_DEBUG`
- `DJANGO_SECRET_KEY`
- `DJANGO_ALLOWED_HOSTS`
- `DB_NAME`: Required
- `DB_USER`: Required
- `DB_PASSWORD`: Required
- `DB_ADDRESS`: Required
- `DB_PORT`
- `DJANGO_STATIC_ROOT`
- `SOCIAL_AUTH_LOGIN_REDIRECT_URL`
- `SOCIAL_AUTH_GITHUB_KEY`: Required
- `SOCIAL_AUTH_GITHUB_SECRET`: Required
- `GITHUB_WEB_HOOK_SECRET`: Required
- `SOCIAL_AUTH_GITLAB_KEY`: Required
- `SOCIAL_AUTH_GITLAB_SECRET`: Required
- `SOCIAL_AUTH_GITLAB_REDIRECT_URL`: Required

!!! warning
    It is mandatory to set the variables marked as `Required`.

See [GitMate-2 Environment Variables and How to set them?](./Environment_Variables)

#### Running Development Server

Now we can run GitMate 2. Make sure the virtual environment is available and activated.

To start the development server run

```bash
$ python3 start_debug_server.py
```

Development server is now running at port `8000`.
Visit [https://localhost:8000/docs](https://localhost:8000/docs) from your favourite browser to see GitMate-2 API documentation.

## Setup Gitmate-2 Frontend

GitMate-2 frontend is generated using [Angular CLI](https://github.com/angular/angular-cli).

#### Development Requirements

Make sure you have `npm` (JavaScript package manager) and nodejs(>=6.x) installed on your system.

To install [Angular CLI](https://cli.angular.io/) run (root privileges may be required)

```bash
$ npm install -g @angular/cli
```

Move to `gitmate-2-frontend` directory and run

```bash
$ npm install
```

to install `gitmate-2-frontend` dependencies.

#### Running Development Server

To start a development server, run

```bash
$ ng serve
```

Navigate to [https://localhost:4200/](https://localhost:4200) from you browser.
The app automatically detects changes made to the source files.
