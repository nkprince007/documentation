# Using Docker in Docker

!!! warning
    Make sure you have acknowledged the
    [risks of using Docker in Docker][dind risks] before doing this.

In this documentation, we're assuming you have a working GitMate setup. If not,
Please refer to
[Starting your own instance](../Admins/Starting_your_own_instance).

This may vary on how you set it up, we are going to provide you with two
common setups.

## Using `docker-compose.yml` or `docker-stack.yml`

If you're already using Compose or Stack, you can do the following:

* Open the Stack or Compose file on your text editor
* Create a new service for dind, here's an example
```yaml
...
dind:
  image: docker:stable-dind
  privileged: true
  environment:
    # Make sure you choose the one that works with your current setup.
    #
    # Choose anything other than vfs if possible or you'll be wasting a LOT of
    # hard drive space.
    #
    # Here's a list of storage drivers from the official documentations:
    # https://docs.docker.com/engine/userguide/storagedriver/selectadriver/
    - DOCKER_DRIVER: overlay
...
```
* Remove the "/var/run/docker.sock" mount from the gitmate worker service
* Add a link to the gitmate worker service, here's an example
```yaml
...
worker:
  build: .
  command: ./docker/run-worker.sh
  environment:
    ...
    - DOCKER_HOST=tcp://dind:2375
  links:
    ...
    - dind
...
```
* Redeploy your compose/stack setup and you should be good to go.

## Using `docker service`

Sadly, We can't run Docker in Docker with this method at the moment because
`docker service` cannot create privileged containers which are needed by dind.

You can follow the issue for this
[here](https://github.com/moby/moby/issues/24862).

[dind risks]: https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/
