# Handling Stale Issues

GitMate helps to prevent cruft from accumulating in your repository by exclusively labelling issues that haven't seen work or updates in a long time.
Based on the labels set on issue, actions can be performed on issues including posting a message, unassigning inactive developers, and eventually closing the issue.

To use the feature activate `Label inactive issues` plugin on your repository.

![image](../images/stale_plugin.png)

Number of days for an issue to be considered inactive and the label to be added can be easily modified.

