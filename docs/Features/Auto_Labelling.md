# Automatic Label Setting

GitMate automatically sets appropriate labels on newly opened issues based on the conventions
you are already using. Gitmate learns from labels you have used in past and continuously
improves.

To use this feature you need to [activate GitMate on your repository](../Users/Getting_Started#2-activate-gitmate-2-on-a-repo)
and use `Finds similar issues` plugin.
Activate `Assign related labels` setting. GitMate will now add predicted labels to newly opened issues on your repository.

![image](../images/issue_label_prediction.png)

You can also set certain labels to be excluded from GitMate's prediction.
Just provide a comma separated list of labels to be excluded.

Furthermore, keywords can be set up to trigger specific labels.
GitMate looks for keywords in issue title and body, and add specified labels.
Use `Assign mentioned labels` plugin for this feature.

![image](../images/auto_issue_labeller.png)
